from flask import Flask, request
from flask_restful import Resource, Api
from json import dumps
from flask import jsonify

app = Flask(__name__)
api = Api(app)

class Index(Resource):
  def get(self):
    return {'Hello': 'World!'} # Fetches first column that is Employee ID

api.add_resource(Index, '/api') # Route_1
if __name__ == '__main__':
     app.run(port='5002')